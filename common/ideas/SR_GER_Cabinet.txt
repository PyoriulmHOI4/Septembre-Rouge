#### Add new sections corresponding to the idea slot for adding ministers to new slots

ideas = {
	#################################################
	### Head of Government
	#################################################
	Head_of_Government = {
		# Eugen Schiffer
		GER_HoG_Eugen_Schiffer = {
			picture = Eugen_Schiffer
			allowed = { tag = GER }
			allowed_to_remove = { always = no }
			available = {
				date > 1932.12.30
				date < 1960.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Eugen_Schiffer_unavailable }
			}
			
			traits = { ideology_D POSITION_GER_Reichskanzler HoG_Old_Lawyer }
		}
		# Johannes Popitz
		GER_HoG_Johannes_Popitz = {
			picture = Johannes_Popitz
			allowed = { tag = GER }
			allowed_to_remove = { always = no }
			available = {
				date > 1932.12.30
				date < 1960.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Johannes_Popitz_unavailable }
			}
			
			traits = { ideology_D POSITION_GER_Reichskanzler HoG_Old_Lawyer }
		}
	}
}
