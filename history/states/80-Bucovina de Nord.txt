state={
	id=80
	name="STATE_80"
	manpower = 474600
	
	state_category = rural
	
	history= {
		owner = AUS
		victory_points = {
			9548 2
		}
		buildings = {
			infrastructure = 6
			arms_factory = 1
		}
		add_core_of = AUS	
	}
	provinces={
		577 9548
	}
}
