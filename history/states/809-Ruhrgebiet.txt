
state={
	id=809
	name="STATE_809"
	resources={
		steel=14.000
	}

	history={
		owner = GER
		victory_points = {
			6469 10 
		}
		buildings = {
			infrastructure = 8
			arms_factory = 1
			industrial_complex = 1
			air_base = 3

		}
		add_core_of = GER

	}

	provinces={
		3512 6469 6570 9482 
	}
	manpower=3584433
	buildings_max_level_factor=1.000
	state_category=large_town
}
